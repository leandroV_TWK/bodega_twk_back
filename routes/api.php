<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\BodegaController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ComunaController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\RolController;
use App\Http\Controllers\SucursalController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContrasenaController;
use App\Http\Controllers\TipoPagoController; 
use App\Http\Controllers\VentaController;
use App\Http\Controllers\ProductoVentaController;
use Illuminate\Support\Facades\Password;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('/login',[AuthController::class,'login']);
Route::post('/correo',[ContrasenaController::class, 'recuperarContrasena']);

Route::group(['middleware' => ['auth:sanctum']], function(){
    
    Route::group(['middleware' => 'admin'], function () {
        
        Route::resource('rol', RolController::class);
        Route::resource('usuario', UserController::class);

        Route::get('/pie', [ProductoController::class, 'pie']);
        Route::get('/barra', [ProductoController::class, 'barra']);
        Route::get('totalpro', [ProductoController::class, 'cantidad']);
        Route::get('totalbod', [BodegaController::class, 'cantidad']);
        Route::get('totalusu', [UserController::class, 'cantidad']);
        Route::get('infousuario/{id}', [UserController::class, 'infoUsuario']);
        Route::get('infosucursal/{id}', [SucursalController::class, 'infoSucursal']);
        Route::get('infobodega/{id}', [BodegaController::class, 'infoBodega']);
        Route::get('infoproducto/{id}', [ProductoController::class, 'infoProducto']);
        Route::get('totalsuc', [SucursalController::class, 'cantidad']);
        Route::get('totalcat', [CategoriaController::class, 'cantidad']);        
    });

    Route::group(['middleware' => 'jefe'], function () {
        Route::resource('comuna', ComunaController::class);
        Route::resource('region', RegionController::class);
        Route::resource('sucursal', SucursalController::class);
    });

    Route::group(['middleware' => 'opera'], function () {
        Route::resource('bodega', BodegaController::class);
        Route::resource('categoria', CategoriaController::class);
        Route::resource('producto', ProductoController::class);
    });

    Route::group(['middleware' => 'vendedor'], function () {
        Route::get('procat/{id}', [ProductoController::class, 'productoCategoria']);
        Route::resource('/categoria', CategoriaController::class);
        Route::resource('/proven', ProductoVentaController::class);
        Route::resource('/venta', VentaController::class);
        Route::resource('/tipopago', TipoPagoController::class);
    });

    Route::post('/logout',[AuthController::class,'logout']);
    Route::post('/usuactual',[AuthController::class,'usuarioActual']);

});