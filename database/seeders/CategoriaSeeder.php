<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('categorias')->insert([
            'nombre' => 'Computación',
            'descripcion' => 'Computadoras y artículos',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Hogar',
            'descripcion' => 'Linea blanca y otros',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Vestuario',
            'descripcion' => 'Ropa y calzado',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Higiene',
            'descripcion' => 'Higiene personal',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
