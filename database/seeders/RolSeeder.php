<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('roles')->insert([            
            'descripcion' => 'Administrador',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        $now = Carbon::now();
        DB::table('roles')->insert([            
            'descripcion' => 'Jefe de bodega',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        $now = Carbon::now();
        DB::table('roles')->insert([            
            'descripcion' => 'Operario de bodega',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        $now = Carbon::now();
        DB::table('roles')->insert([            
            'descripcion' => 'Vendedor',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
