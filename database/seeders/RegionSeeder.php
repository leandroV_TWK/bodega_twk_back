<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('regiones')->insert([            
            'nombre' => 'Araucanía',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        $now = Carbon::now();
        DB::table('regiones')->insert([            
            'nombre' => 'Biobío',
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
