<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(RegionSeeder::class);
        $this->call(ComunaSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(SucursalSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CategoriaSeeder::class);
        $this->call(BodegaSeeder::class);
        $this->call(ProductoSeeder::class);
        $this->call(TipoPagoSeeder::class);
        
    }
}
