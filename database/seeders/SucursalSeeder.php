<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class SucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('sucursales')->insert([            
            'nombre' => 'Sucursal 1',
            'descripcion' => ' Destinada a productos frágiles',
            'comuna_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
