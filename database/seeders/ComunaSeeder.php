<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class ComunaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('comunas')->insert([            
            'nombre' => 'Temuco',
            'region_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        $now = Carbon::now();
        DB::table('comunas')->insert([            
            'nombre' => 'Talcahuano',
            'region_id' => 2,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
