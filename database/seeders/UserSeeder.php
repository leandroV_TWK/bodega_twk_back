<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('users')->insert([            
            'name' => 'Leandro',
            'email' => 'leandro.vargas2020@twk.cl',
            'email_verified_at' => null,
            'password' => bcrypt('123'),
            'rol_id' => 1,
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('users')->insert([            
            'name' => 'Leandro 2',
            'email' => 'jefe@bodega.com',
            'email_verified_at' => null,
            'password' => bcrypt('456'),
            'rol_id' => 2,
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('users')->insert([            
            'name' => 'Leandro 3',
            'email' => 'operario@bodega.com',
            'email_verified_at' => null,
            'password' => bcrypt('789'),
            'rol_id' => 3,
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('users')->insert([            
            'name' => 'Leandro 4',
            'email' => 'vendedor@bodega.com',
            'email_verified_at' => null,
            'password' => bcrypt('789'),
            'rol_id' => 4,
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
