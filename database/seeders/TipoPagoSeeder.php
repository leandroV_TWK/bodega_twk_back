<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;

class TipoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('tipo_pagos')->insert([            
            'descripcion' => 'Efectivo',            
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        $now = Carbon::now();
        DB::table('tipo_pagos')->insert([            
            'descripcion' => 'Tarjeta de débito',            
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        $now = Carbon::now();
        DB::table('tipo_pagos')->insert([            
            'descripcion' => 'Tarjeta de crédito',            
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
