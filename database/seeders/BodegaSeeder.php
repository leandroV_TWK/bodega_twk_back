<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class BodegaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('bodegas')->insert([
            'codigo' => 'ch33k4',
            'nombre' => 'Bodega 1',
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('bodegas')->insert([
            'codigo' => 'ch3764',
            'nombre' => 'Bodega 2',
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('bodegas')->insert([
            'codigo' => 'ch77k4',
            'nombre' => 'Bodega 3',
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('bodegas')->insert([
            'codigo' => 'ch434',
            'nombre' => 'Bodega 4',
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('bodegas')->insert([
            'codigo' => 'ch453674',
            'nombre' => 'Bodega 5',
            'sucursal_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
