<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('productos')->insert([
            'codigo' => 'hseer423',
            'nombre' => 'Notebook Dell 15',
            'descripcion' => 'Notebook con un procesador AMD de 3 GHZ',
            'cantidad' => 50,
            'precio' => 1000,
            'categoria_id' => 1,
            'bodega_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'hst7623',
            'nombre' => 'Cepillo de dientes',
            'descripcion' => 'Cepillo con 1000 hebras',
            'cantidad' => 40,
            'precio' => 1000,
            'categoria_id' => 4,
            'bodega_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'hfdr423',
            'nombre' => 'Mesa',
            'descripcion' => 'Mesa para 40 personas',
            'cantidad' => 60,
            'precio' => 1000,
            'categoria_id' => 2,
            'bodega_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'h4er423',
            'nombre' => 'Zapato verde',
            'descripcion' => 'Zapato para usuarios de Instagram',
            'cantidad' => 70,
            'precio' => 1000,
            'categoria_id' => 3,
            'bodega_id' => 4,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'hsee5423',
            'nombre' => 'Cocina Espacial',
            'descripcion' => 'Cocina con cohete',
            'cantidad' => 20,
            'precio' => 1000,
            'categoria_id' => 2,
            'bodega_id' => 1,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'hse4323',
            'nombre' => 'Teclado Logitech',
            'descripcion' => 'Teclado que se escribe solo',
            'cantidad' => 100,
            'precio' => 1000,
            'categoria_id' => 1,
            'bodega_id' => 3,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'h7773',
            'nombre' => 'Raspberry py 3+',
            'descripcion' => 'Mini pc',
            'cantidad' => 30,
            'precio' => 40000,
            'categoria_id' => 1,
            'bodega_id' => 2,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        DB::table('productos')->insert([
            'codigo' => 'h69273',
            'nombre' => 'Monitor Coreano',
            'descripcion' => 'Monitor 200 pulgadas 80k',
            'cantidad' => 40,
            'precio' => 40000,
            'categoria_id' => 1,
            'bodega_id' => 5,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
