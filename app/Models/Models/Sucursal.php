<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Models\Comuna;

class Sucursal extends Model
{
    protected $table = 'sucursales';
    protected $primaryKey = 'id';
    protected $fillable = [        
        'nombre',
        'descripcion',
        'comuna_id',
    ];

    public function comunas(){
        return $this->hasMany(Comuna::class, 'id');
    }
}
