<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{
    protected $table = 'comunas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre',
        'region_id',
    ];

    public function regiones(){
        return $this->hasMany(Region::class, 'id');
    }
}
