<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Models\Sucursal;

class Bodega extends Model
{

    protected $table = 'bodegas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'codigo',
        'nombre',
        'sucursal_id',
    ];

    public function Sucursales(){
        return $this->hasMany(Sucursal::class, 'id');
    }
}
