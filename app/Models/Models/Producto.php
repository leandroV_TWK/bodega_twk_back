<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Models\Categoria;
use App\Models\Models\Bodega;

class Producto extends Model
{
    protected $table = 'productos';
    protected $primaryKey = 'id';
    protected $fillable = [
        'codigo',
        'nombre',
        'descripcion',
        'cantidad',
        'precio',
        'categoria_id',
        'bodega_id',
    ];

    public function categorias(){
        return $this->haasMany(Categoria::class, 'id');
    }
    public function bodegas(){
        return $this->hasMany(Bodega::class, 'id');
    }
}
