<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoVenta extends Model
{
    protected $table = 'productos_ventas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'producto_id',
        'venta_id',
        'cantidad',
        'total_venta',    
    ];
}
