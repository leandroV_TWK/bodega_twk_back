<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->check() && auth()->user()->rol_id == 1){
            return $next($request);
        }else{
            return response()->json([
                'status_code' => 400,
                'message' => 'Usuario sin autorización'
            ]);
           }
    }
}
