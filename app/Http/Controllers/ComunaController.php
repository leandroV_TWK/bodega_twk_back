<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\Comuna;

class ComunaController extends Controller
{
    public function index()
    {
        return Comuna::all();
    }

}
