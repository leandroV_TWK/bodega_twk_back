<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function store(Request $request)
    {
        $request['password'] = bcrypt($request->input('password'));
        User::create($request->all());   

        return ['created' => true];
    }

    public function show($id)
    {
        return User::find($id);
        
    }

    public function infoUsuario($id){

        $usuario = DB::table('users')
        ->addSelect(DB::raw('users.id'))
        ->addSelect(DB::raw('users.name'))
        ->addSelect(DB::raw('users.email'))
        ->addSelect(DB::raw('users.updated_at'))
        ->addSelect(DB::raw('sucursales.nombre as sucursal'))
        ->addSelect(DB::raw('roles.descripcion as rol'))
		->from('users')
		->join('roles', function($join) {
			$join->on('users.rol_id', '=', 'roles.id');
            })
        ->join('sucursales', function($join) {
            $join->on('users.sucursal_id', '=', 'sucursales.id');
            })
        ->where('users.id', '=', $id)
		->get();
            
        return $usuario;
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());
        return ['updated' => true];
    }

    public function destroy($id)
    {
        User::destroy($id);
        return ['deleted' => true];
    }
    public function cantidad(){
        $cantidad = DB::table('users')->count();
        return ['total' => $cantidad];
    }

}
