<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\Producto;
use App\Models\Models\Categoria;
use DB;

class ProductoController extends Controller
{
    public function index()
    {
        return Producto::all();
    }

    public function store(Request $request)
    {
        Producto::create($request->all());
        return ['created' => true];
    }

    public function show($id)
    {
        return Producto::find($id);
    }

    public function infoProducto($id){

        $producto = DB::table('productos')
        ->addSelect(DB::raw('productos.id'))
        ->addSelect(DB::raw('productos.codigo'))
        ->addSelect(DB::raw('productos.nombre'))
        ->addSelect(DB::raw('productos.descripcion'))
        ->addSelect(DB::raw('productos.cantidad'))
        ->addSelect(DB::raw('productos.precio'))
        ->addSelect(DB::raw('productos.updated_at'))
        ->addSelect(DB::raw('categorias.nombre as categoria'))
        ->addSelect(DB::raw('bodegas.nombre as bodega'))
		->from('productos')
		->join('categorias', function($join) {
			$join->on('productos.categoria_id', '=', 'categorias.id');
            })
        ->join('bodegas', function($join) {
            $join->on('productos.bodega_id', '=', 'bodegas.id');
            })
        ->where('productos.id', '=', $id)
		->get();
            
        return $producto;
    }

    public function update(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->update($request->all());
        return ['updated' => true];
    }

    public function destroy($id)
    {
        Producto::destroy($id);
        return ['deleted' => true];
    }
    public function pie()
    {
        $datosPie = DB::table('productos')
        ->addSelect(DB::raw('bodegas.nombre'))
		->addSelect(DB::raw('SUM(productos.cantidad) as cantidad'))
		->from('productos')
		->join('bodegas', function($join) {
			$join->on('productos.bodega_id', '=', 'bodegas.id');
            })
        ->orderBy('cantidad', 'DESC')
		->groupBy('bodegas.nombre')
		->limit(4)
		->get();

        return $datosPie;
    } 
    public function barra()
    {
        $datosBarra = DB::table('productos')
        ->addSelect(DB::raw('categorias.nombre'))
		->addSelect(DB::raw('SUM(productos.cantidad) as cantidad'))
		->from('productos')
		->join('categorias', function($join) {
			$join->on('productos.categoria_id', '=', 'categorias.id');
            })
        ->orderBy('cantidad', 'DESC')
		->groupBy('categorias.nombre')
		->limit(4)
		->get();

        return $datosBarra;
    }
    public function cantidad(){
        $cantidad = DB::table('productos')
        ->addSelect(DB::raw('count(productos.id) as total'))
        ->addSelect(DB::raw('SUM(productos.cantidad) as cantidad'))
        ->get();
        return $cantidad;
    }
    public function productoCategoria($id){                        
        $productos = DB::table('productos')                
                ->where('categoria_id','=', $id)
                ->get();
        return $productos;

        /*
        $productos = Producto::all()
        -> */
            }
}