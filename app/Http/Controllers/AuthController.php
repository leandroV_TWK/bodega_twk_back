<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function usuarioActual(){

        $usuario = Auth::user();

        if(!$usuario->name){
            return response()->json([
                'status_code' => 400,
                'message' => 'Bad Request' ]);
        }

            return response()->json($usuario,200);
    }

    public function login(Request $request){

        $validar = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);
    
        if($validar->fails()){
            return response()->json([
                'status_code' => 400,
                'message' => 'Bad Request' ]);
        }
    
        $credenciales = request(['email','password']);
    
        if(!Auth::attempt($credenciales)){
            return response()->json([
                'status_code' => 500,
                'message' => 'No autorizado'
            ]);
        }
    
        $usuario = User::where('email', $request->email)->first();
    
        $token = $usuario->createToken('authToken')->plainTextToken;
    
        return response()->json([
            'status_code' => 200,
            'token' => $token,
            'tipo' => $usuario->rol_id
        ]);
    }
    
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'status_code' => 200,
            'message' => 'token eliminado'
        ]);
    }

}
