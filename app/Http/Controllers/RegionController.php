<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\Region;
class RegionController extends Controller
{
    public function index()
    {
        return Region::all();
    }
}
