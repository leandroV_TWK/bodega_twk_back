<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\Sucursal;
use DB;

class SucursalController extends Controller
{
    public function index()
    {
        return Sucursal::all();
    }

    public function store(Request $request)
    {
        Sucursal::create($request->all());
        return ['created' => true];
    }

    public function show($id)
    {
        return Sucursal::find($id);
    }

    public function infoSucursal($id){

        $sucursal = DB::table('sucursales')
        ->addSelect(DB::raw('sucursales.id'))
        ->addSelect(DB::raw('sucursales.nombre'))
        ->addSelect(DB::raw('sucursales.descripcion'))
        ->addSelect(DB::raw('sucursales.updated_at'))
        ->addSelect(DB::raw('comunas.nombre as comuna'))
		->from('sucursales')
		->join('comunas', function($join) {
			$join->on('sucursales.comuna_id', '=', 'comunas.id');
            })
        ->where('sucursales.id', '=', $id)
		->get();
            
        return $sucursal;
    }

    public function update(Request $request, $id)
    {
        $sucursal = Sucursal::find($id);
        $sucursal->update($request->all());
        return ['updated' => true];
    }

    public function destroy($id)
    {
        Sucursal::destroy($id);
        return ['deleted' => true];
    }
    public function cantidad(){
        $cantidad = DB::table('sucursales')->count();
        return ['total' => $cantidad];
    }
}
