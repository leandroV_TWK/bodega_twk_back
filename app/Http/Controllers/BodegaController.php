<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\Bodega;
use DB;

class BodegaController extends Controller
{
    public function index()
    {
        return Bodega::all();
    }

    public function store(Request $request)
    {
        Bodega::create($request->all());
        return ['created' => true];
    }

    public function show($id)
    {
        return Bodega::find($id);
    }

    public function infoBodega($id){

        $bodega = DB::table('bodegas')
        ->addSelect(DB::raw('bodegas.id'))
        ->addSelect(DB::raw('bodegas.codigo'))
        ->addSelect(DB::raw('bodegas.nombre'))
        ->addSelect(DB::raw('bodegas.updated_at'))
        ->addSelect(DB::raw('sucursales.nombre as sucursal'))
		->from('bodegas')
		->join('sucursales', function($join) {
			$join->on('bodegas.sucursal_id', '=', 'sucursales.id');
            })
        ->where('bodegas.id', '=', $id)
		->get();
            
        return $bodega;
    }

    public function update(Request $request, $id)
    {
        $bodega = Bodega::find($id);
        $bodega->update($request->all());
        return ['updated' => true];
    }

    public function destroy($id)
    {
        Bodega::destroy($id);
        return ['deleted' => true];
    }
    public function cantidad(){
        $cantidad = DB::table('bodegas')->count();
        return ['total' => $cantidad];
    }
}
