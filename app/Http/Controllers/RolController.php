<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Models\Rol;

class RolController extends Controller
{
    public function index()
    {
        return Rol::all();
    }
    public function show($id)
    {
        return Rol::find($id);
    }
}
