<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductoVenta;
use DB;

class ProductoVentaController extends Controller
{
    public function store(Request $request)
    {
        $datos = $request->all();      
        
        foreach($datos as $criterio){                    
            $producto_id = $criterio["producto_id"];
            $venta_id = $criterio["venta_id"];
            $cantidad = $criterio["cantidad"];
            $total_venta = $criterio["total_venta"];         
        
            DB::table('productos_ventas')->insert([
                'producto_id' => $producto_id,
                'venta_id' => $venta_id,
                'cantidad' => $cantidad,
                'total_venta' => $total_venta
            ]);
        }
          
        return ['created' => true];
    }
}
