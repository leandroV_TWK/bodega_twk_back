<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContrasenaMail;
use Illuminate\Support\Facades\Crypt;

class ContrasenaController extends Controller
{
    public function recuperarContrasena(Request $request){
        $validar = Validator::make($request->all(),[
            'email' => 'required|email'            
        ]);
        if($validar->fails()){
            return response()->json([
                'status_code' => 400,
                'message' => 'Bad Request' ]);
        }
        //confirmar correo
        $usuario = User::where('email', $request->email)->first();

        if($usuario!=null){
            $contrasena = decrypt($usuario->password);
            $correo = $usuario->email;
        }

       //enviar correo
       $details = [
        'title' => 'Sistema de bodegas LV',
        'body' => 'Estimado ' . $usuario->name . ', su clave usuario es: ' . $contrasena
    ];
    if($usuario!=null){
    Mail::to($correo)->send(new ContrasenaMail($details));
       
            return response()->json([
                'status_code' => 200,
                'message' => "El correo ha sido enviado exitosamente"
            ]);
    } else{
        return response()->json([
            'status_code' => 404,
            'message' => "El correo no está registrado"
        ]);
    }
    }
}
