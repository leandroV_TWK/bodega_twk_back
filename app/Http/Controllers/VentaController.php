<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Venta;
use DB;
class VentaController extends Controller
{
    public function store(Request $request)
    {        
        Venta::create($request->all());   
        
        $cantidad = DB::table('ventas')
        ->addSelect(DB::raw('count(ventas.id) as ventas'))        
        ->get();

        if($cantidad){
        $venta = $cantidad[0]->ventas;}
                
        return response()->json([
            'created' => true,
            'venta' => $venta
        ]);
    }
}
