<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Tu contraseña se ha actualizado!',
    'sent' => 'Hemos enviado un correo para que actualices tu contraseña!',
    'throttled' => 'Por favor esperar un poco antes de volver a intentar.',
    'token' => 'El token para cambiar la contraseña es inválido.',
    'user' => "No hemos encontrado un usuario vinculado a ese correo.",

];
