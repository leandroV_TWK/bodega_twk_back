<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Kodinger">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Reset Password</title>
	<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/my-login.css') }}">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body class="my-login-page">
	<section class="h-100">
		<div class="container h-100">
			<div class="row justify-content-md-center align-items-center h-100">
				<div class="card-wrapper" style="text-align:center;margin-top:100px">
				
					<div class="cardx fat">
					<img src="{{ asset('img/Logo.png') }}" alt="Sistema de Bodegas LV" style="width:200px">
						<div class="card-body">
							<h4 class="card-title">Cambiar contraseña</h4>
							<form method="POST" class="my-login-validation" action="{{ route('password.update') }}">
                                @csrf

								<input type="hidden" name="token" value="{{ $request->route('token') }}">
                                
								<div class="form-group">
									<label for="email">Correo</label>
									<x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email', $request->email)" required />
                                    <br>
									<span class="text-danger">@error('email'){{$message}} @enderror</span>
								</div>
								<br>
								<div class="form-group">
									<label for="password">Nueva Contraseña</label>
									<input id="password" style="width:250px;margin-left:auto;margin-right:auto" type="password" class="form-control" name="password" placeholder="Ingrese su nueva contraseña">
                                    <span class="text-danger">@error('password'){{$message}}@enderror</span>
								</div>
								<br>
								<div class="form-group">
									<label for="password-confirm">Confirmar Contraseña</label>
									<input id="password-confirm" style="width:250px;margin-left:auto;margin-right:auto" type="password" class="form-control" name="password_confirmation" placeholder="Ingrese nuevamente la contraseña">
                                    <span class="text-danger">@error('password_confirmation'){{$message}} @enderror</span>
								</div>
								<br>
								<div class="form-group m-0">
									<button type="submit" class="btn btn-success btn-block">
									{{ __('Reset Password') }}
									</button>
								</div>
							</form>
						</div>
					</div>
					<div class="footer">
						Copyright &copy; 2021 &mdash; Sistema de Bodegas LV
					</div>
				</div>
			</div>
		</div>
	</section>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
